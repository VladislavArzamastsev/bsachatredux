import Chat from './src/component/chat/Chat';
import { rootReducer } from './src/reducer/rootReducer';

export default {
    Chat,
    rootReducer,
};