import ActionName from "../actions/action-name";

let initialState = {
    chat: {
        messages: [],
        editModal: false,
        preloader: true,
        editedMessage: undefined,
        lastMessageDate: undefined,
        participantsCount: 0,
        chatName: undefined
    },
    user: {
        likedMessagesIds: new Set(),
        currentUserId: undefined,
        currentAvatar: undefined,
        currentUserName: undefined
    }
}

function rootReducer(
    currentState = initialState,
    action) {
    switch (action.type) {
        case ActionName.SET_CHAT_DATA:
            return {
                ...currentState,
                chat: action.payload
            }
        case ActionName.DELETE_MESSAGE:
            return {
                ...currentState,
                chat: {
                    ...currentState.chat,
                    messages: action.payload.messages,
                    lastMessageDate: action.payload.lastMessageDate,
                    participantsCount: action.payload.participantsCount,
                }
            }
        case ActionName.LIKE_MESSAGE:
            return {
                ...currentState,
                chat: {
                    ...currentState.chat,
                    messages: action.payload.messages,
                    lastMessageDate: action.payload.lastMessageDate,
                    participantsCount: action.payload.participantsCount,
                }
            }
        case ActionName.CREATE_MESSAGE:
            return {
                ...currentState,
                chat: {
                    ...currentState.chat,
                    messages: action.payload.messages,
                    lastMessageDate: action.payload.lastMessageDate,
                    participantsCount: action.payload.participantsCount
                }
            }
        case ActionName.DISPLAY_EDIT_MODAL:
            return {
                ...currentState,
                chat: {
                    ...currentState.chat,
                    editModal: action.payload.editModal,
                    editedMessage: action.payload.editedMessage
                }
            }
        case ActionName.EDIT_MESSAGE:
            return {
                ...currentState,
                chat: {
                    ...currentState.chat,
                    messages: action.payload.messages,
                    editModal: false,
                    editedMessage: undefined,
                }
            }
        case ActionName.SET_USER_INFO:
            return {
                ...currentState,
                user: action.payload
            }
        default:
            return currentState;
    }
}

export {rootReducer}
