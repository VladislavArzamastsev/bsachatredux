import React from 'react';
import ReactDOM from 'react-dom';
import { store } from './store/store';
import { Provider } from 'react-redux';
import Chat from "./component/chat/Chat";

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
        <Chat
            url='https://edikdolynskyi.github.io/react_sources/messages.json'
            chatName="My chat"
            currentUserId="9e243930-83c9-11e9-8e0c-8f1a686f4ce4"
            currentAvatar="https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA"
            currentUserName="Ruth"
        />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

