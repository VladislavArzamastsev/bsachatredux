import {deleteMessage, likeMessage, createMessage, displayEditModal, editMessage} from "../actions/actions";
import {v4 as uuidv4} from 'uuid';
import MessageEntity from "../entity/message-entity";

function performDeleteMessage(messages, messageId, dispatch) {
    if (messages === undefined || messages === []) {
        return;
    }
    const filtered = messages.filter(m => m.id !== messageId);
    const payload = {
        messages: filtered,
        lastMessageDate: filtered[filtered.length - 1]?.createdAt,
        participantsCount: new Set(filtered.map(m => m.userId)).size
    };
    dispatch(deleteMessage(payload));
}

function performMessageLike(messages, likedMessagesIds, messageId, lastMessageDate, participantsCount, dispatch) {
    let delta;
    if (likedMessagesIds.delete(messageId)) {
        // It means, user already liked message
        delta = -1;
    } else {
        delta = 1;
        likedMessagesIds.add(messageId);
    }
    const mapped = messages.map(m => m.id !== messageId ? m : {
        ...m,
        likeCount: m.likeCount + delta
    });
    const payload = {
        messages: mapped,
        lastMessageDate: lastMessageDate,
        participantsCount: participantsCount,
    };
    dispatch(likeMessage(payload));
}

function performCreateMessage(messages, messageText, currentUserId, currentAvatar, currentUserName, dispatch) {
    const id = uuidv4();
    const now = new Date();
    const entity = new MessageEntity(id, currentUserId, currentAvatar, currentUserName,
        messageText, now, undefined, 0);
    const newMessages = [...messages, entity];
    const payload = {
        messages: newMessages,
        lastMessageDate: now,
        participantsCount: new Set(newMessages.map(m => m.userId)).size
    };
    dispatch(createMessage(payload));
}

function performShowingModal(messageToEdit, dispatch) {
    const payload = {
        editModal: true,
        editedMessage: messageToEdit,
    };
    dispatch(displayEditModal(payload));
}

function performHidingModal(dispatch) {
    const payload = {
        editModal: false,
        editedMessage: undefined,
    };
    dispatch(displayEditModal(payload));
}

function performUpdating(messages, messageId, newText, dispatch) {
    const payload = {
        messages: messages.map(m => m.id !== messageId ? m : {
            ...m,
            text: newText,
            editedAt: new Date()
        })
    }
    dispatch(editMessage(payload));
}

export {
    performDeleteMessage,
    performMessageLike,
    performCreateMessage,
    performShowingModal,
    performHidingModal,
    performUpdating
}