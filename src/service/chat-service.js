import { setChatData, setUserInfo } from "../actions/actions";
import MessageEntity from "../entity/message-entity";


function mapResponseEntryToMessageEntity(entry) {
    return new MessageEntity(entry.id, entry.userId, entry.avatar, entry.user, entry.text,
        new Date(entry.createdAt), new Date(entry.editedAt),  0);
}

function createChatData(messages, chatName){
    return {
        messages: messages,
        lastMessageDate: messages[messages.length - 1]?.createdAt,
        participantsCount: new Set(messages.map(m => m.userId)).size,
        editedMessage: undefined,
        editModal: false,
        preloader: false,
        chatName: chatName
    }
}

async function loadChatData(url, chatName, dispatch) {
    const dataJson = await fetch(url)
        .then(resp => resp.json());
    const chatData = createChatData(dataJson
        .map(entry => mapResponseEntryToMessageEntity(entry)), chatName);
    dispatch(setChatData(chatData));
}

function setCurrentUserInfo(likedMessagesIds, currentUserId,
                            currentAvatar, currentUserName, dispatch) {
    const user = {
        likedMessagesIds: likedMessagesIds,
        currentUserId: currentUserId,
        currentAvatar: currentAvatar,
        currentUserName: currentUserName
    };
    dispatch(setUserInfo(user));
}

export { loadChatData, setCurrentUserInfo };