import * as React from 'react';
import './header.css';
import {getFormattedDate} from '../../formatter/date-time-formatter';
import {useSelector} from 'react-redux';

const Header = () => {
    const {chatName, participantsCount, messageCount, lastMessageDate} = useSelector(state => ({
        chatName: state.chat.chat.chatName,
        participantsCount: state.chat.chat.participantsCount,
        messageCount: state.chat.chat.messages.length,
        lastMessageDate: state.chat.chat.lastMessageDate,
    }));

    return (
        <div className="header">
            <div className="header-title">
                {chatName}
            </div>
            <div className="header-users-count">
                {participantsCount} Participants
            </div>
            <div className="header-messages-count">
                {messageCount} Messages
            </div>
            <div className="header-last-message-date">
                {getFormattedDate(lastMessageDate, "dd.mm.yyyy HH:MM")}
            </div>
        </div>
    );
}

export default Header;