import * as React from 'react';
import './edit.css';
import { performHidingModal, performUpdating } from '../../service/message-service';
import { useDispatch, useSelector } from 'react-redux';

const Edit = () => {

    const { isShown, editedMessage, messages, } = useSelector(state => ({
        isShown: state.chat.chat.editModal,
        editedMessage: state.chat.chat.editedMessage,
        messages: state.chat.chat.messages
    }));

    const [text, setText] = React.useState("");

    const dispatch = useDispatch();

    const onClose = () => {
        setText("");
        performHidingModal(dispatch);
    }

    const onEdit = () => {
        if(text === undefined || text === ""){
            return;
        }
        performUpdating(messages, editedMessage.id, text, dispatch);
        onClose();
    }

    return (
        <div className={`edit-message-modal ${ isShown ? "modal-shown" : ""}`}>
            <div className="edit-header">
                Edit message
            </div>
            <textarea className="edit-message-input"
                      value={text}
                      onChange={(ev) => setText(ev.target.value)}
            />
            <div className="edit-button-holder">
                <button className="edit-message-button" onClick={onEdit}>Save</button>
                <button className="edit-message-close" onClick={onClose}>Cancel</button>
            </div>

        </div>
    );
}

export default Edit;