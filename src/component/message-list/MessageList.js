import * as React from 'react';
import "./messageList.css";
import OwnMessage from "../message/OwnMessage";
import Message from "../message/Message";
import { compareDatesWithoutTime } from "../../utils/date-time-utils";
import {v4 as uuidv4} from 'uuid';

import { useSelector } from 'react-redux';

function mapMessageToAppropriateComponent(m, currentUserId, likedMessagesIds) {
    if (m.userId === currentUserId) {
        return <OwnMessage
            key={uuidv4()}
            message={m}
        />;
    }
    return (
        <Message
            key={uuidv4()}
            liked={likedMessagesIds.has(m.id)}
            message={m}
        />);
}

function getMessageDivider(date) {
    let dividerMessage;
    const today = new Date();
    const yesterday = new Date().setDate(today.getDate() - 1);
    if (compareDatesWithoutTime(date, today) === 0) {
        dividerMessage = "Today";
    } else if (compareDatesWithoutTime(date, yesterday) === 0) {
        dividerMessage = "Yesterday";
    } else {
        const day = date.getDate();
        const longMonth = date.toLocaleString('en-us', {month: 'long'});
        const weekday = new Intl.DateTimeFormat('en-US', {weekday: 'long'}).format(date);
        dividerMessage = weekday + ", " + day + " " + longMonth;
    }
    return (
        <div
            key={uuidv4()}
            className="messages-divider"
        >{dividerMessage}
        </div>
    );
}

function createMessageList(messages, currentUserId, likedMessagesIds) {
    const out = [];
    let previous = undefined;
    let current = undefined;
    for (let i = 0; i < messages.length; i++) {
        current = messages[i];
        if (previous === undefined ||
            (i < messages.length && compareDatesWithoutTime(current.createdAt, previous.createdAt) > 0)) {
            out.push(getMessageDivider(current.createdAt));
        }
        out.push(mapMessageToAppropriateComponent(current, currentUserId, likedMessagesIds));
        previous = current;
    }
    return out;
}

const MessageList = () => {
    const { messages, currentUserId, likedMessagesIds } = useSelector(state => ({
        messages: state.chat.chat.messages,
        currentUserId: state.chat.user.currentUserId,
        likedMessagesIds: state.chat.user.likedMessagesIds
    }));

    return (
        <div className="message-list">
            {createMessageList(messages, currentUserId, likedMessagesIds)}
        </div>
    );
}

export default MessageList;