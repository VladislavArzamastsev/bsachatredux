import * as React from 'react';
import './messageInput.css';

import {useDispatch, useSelector} from 'react-redux';
import {performCreateMessage} from '../../service/message-service';

const MessageInput = () => {

    const {messages, currentUserId, currentAvatar, currentUserName} = useSelector(state => ({
        messages: state.chat.chat.messages,
        currentUserId: state.chat.user.currentUserId,
        currentAvatar: state.chat.user.currentAvatar,
        currentUserName: state.chat.user.currentUserName
    }));

    const [text, setText] = React.useState("");

    const dispatch = useDispatch();

    const onSend = () => {
        if (text === undefined || text === "") {
            return;
        }
        performCreateMessage(messages, text, currentUserId, currentAvatar, currentUserName, dispatch);
        setText("");
    }

    return (
        <div className="message-input">
            <input
                className="message-input-text"
                value={text}
                onChange={(ev) => setText(ev.target.value)}
            />
            <button
                className="message-input-button"
                onClick={onSend}
            >
                Send
            </button>
        </div>
    );
};

export default MessageInput;
