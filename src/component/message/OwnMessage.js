import * as React from 'react';
import "./message.css";
import PropTypes from 'prop-types';
import {getFormattedDate} from '../../formatter/date-time-formatter';
import {faCog, faTrash} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import { performDeleteMessage, performShowingModal } from '../../service/message-service';
import { useDispatch, useSelector } from 'react-redux';

const OwnMessage = ({
                        message
                    }) => {

    const { messages } = useSelector(state => ({
        messages: state.chat.chat.messages
    }));

    const dispatch = useDispatch();

    const onEdit = () => {
        performShowingModal(message, dispatch);
    }

    const onDelete = () => performDeleteMessage(messages, message.id, dispatch);

    return (
        <div className="own-message">
            <div className="message-text">
                {message.text}
            </div>
            <div className="button-holder">
                <button className="message-edit" onClick={onEdit}>
                    <FontAwesomeIcon icon={faCog}/>
                </button>

                <button className="message-delete" onClick={onDelete}>
                    <FontAwesomeIcon icon={faTrash}/>
                </button>

                Likes: {message.likeCount}
            </div>
            <div className="message-time">
                {getFormattedDate(message.createdAt, "HH:MM")}
            </div>
        </div>
    );

}

OwnMessage.propTypes = {
    message: PropTypes.object
}

export default OwnMessage;