import * as React from 'react';
import "./message.css";
import PropTypes from 'prop-types';
import {getFormattedDate} from '../../formatter/date-time-formatter';
import {faThumbsUp} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

import {useDispatch, useSelector} from 'react-redux';
import {performMessageLike} from '../../service/message-service';

const Message = ({
                     message,
                     liked
                 }) => {

    const { messages, likedMessagesIds, lastMessageDate, participantsCount } = useSelector(state => ({
        messages: state.chat.chat.messages,
        likedMessagesIds: state.chat.user.likedMessagesIds,
        lastMessageDate: state.chat.chat.lastMessageDate,
        participantsCount: state.chat.chat.participantsCount
    }));

    const dispatch = useDispatch();

    const onLike = () => performMessageLike(messages, likedMessagesIds, message.id,
        lastMessageDate, participantsCount, dispatch);

    return (
        <div className={`message ${liked ? "message-liked" : ""}`}>
            <div className="avatar-and-nickname-holder">
                <img className="message-user-avatar"
                     src={message.avatar} alt="Avatar"/>
                <p className="message-user-name">
                    {message.user}
                </p>
            </div>
            <div className="message-text">
                {message.text}
            </div>
            <div className="like-holder">
                {message.likeCount}
                <button className="message-like" onClick={onLike}>
                    <FontAwesomeIcon
                        icon={faThumbsUp}
                        className="like-icon"
                    />
                </button>
            </div>
            <div className="message-time">
                {getFormattedDate(message.createdAt, "HH:MM")}
            </div>
        </div>
    );

}

Message.propTypes = {
    message: PropTypes.object,
    liked: PropTypes.bool
}

export default Message;