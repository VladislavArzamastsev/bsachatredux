import * as React from 'react';
import "./preloader.css";
import logo from './logo.svg';

const Preloader = () => {

    return (
        <div className="preloader">
            <img src={logo} className="App-logo" alt="logo"/>
        </div>
    );

}

export default Preloader;