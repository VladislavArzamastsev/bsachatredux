import * as React from 'react';
import "./chat.css";
import Header from '../header/Header';
import PropTypes from 'prop-types';
import MessageList from "../message-list/MessageList";
import MessageInput from "../message-input/MessageInput";
import Preloader from "../preloader/Preloader";

import { useDispatch, useSelector } from 'react-redux';
import { loadChatData, setCurrentUserInfo } from '../../service/chat-service';
import { performShowingModal } from '../../service/message-service';
import Edit from "../edit/Edit";

const Chat = ({
                  url,
                  chatName,
                  currentUserId,
                  currentAvatar,
                  currentUserName
              }) => {
    const { preloader, messages } = useSelector(state => ({
        preloader: state.chat.chat.preloader,
        messages: state.chat.chat.messages
    }));
    const [renderedFirst, setRenderedFirst] = React.useState(true);
    const dispatch = useDispatch();

    if (renderedFirst) {
        setRenderedFirst(false);
        loadChatData(url, chatName, dispatch);
        setCurrentUserInfo(new Set(), currentUserId, currentAvatar, currentUserName, dispatch);
    }

    React.useEffect(() => {
        window.addEventListener('keydown', (e) => {
            const filtered = messages.filter(m => m.userId === currentUserId);
            if(e.keyCode === 38 && filtered.length > 0) {
                performShowingModal(filtered[filtered.length - 1], dispatch);
            }
        });
    });

    if (preloader) {
        return <Preloader/>;
    }
    return (
        <div className="chat">
            <Header />
            <MessageList />
            <MessageInput />
            <Edit/>
        </div>
    );
}

Chat.propTypes = {
    url: PropTypes.string,
    chatName: PropTypes.string,
    currentUserId: PropTypes.string,
    currentAvatar: PropTypes.string,
    currentUserName: PropTypes.string
}

export default Chat;