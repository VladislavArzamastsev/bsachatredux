import ActionName from './action-name';

export const setChatData = (data) => {
    return {
        "type": ActionName.SET_CHAT_DATA,
        "payload": data
    };
}

export const deleteMessage = (data) => {
    return {
        "type": ActionName.DELETE_MESSAGE,
        "payload": data
    };
}

export const likeMessage = (data) => {
    return {
        "type": ActionName.DELETE_MESSAGE,
        "payload": data
    };
}

export const createMessage = (data) => {
    return {
        "type": ActionName.CREATE_MESSAGE,
        "payload": data
    }
}

export const displayEditModal = (data) => {
    return {
        "type": ActionName.DISPLAY_EDIT_MODAL,
        "payload": data
    }
}

export const editMessage = (data) => {
    return {
        "type": ActionName.EDIT_MESSAGE,
        "payload": data
    }
}

export const setUserInfo = (userInfo) => {
    return {
        "type": ActionName.SET_USER_INFO,
        "payload": userInfo
    }
}
