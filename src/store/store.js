import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import { rootReducer } from '../reducer/rootReducer'

export const store = configureStore({
    reducer: {
        chat: rootReducer
    },
    middleware: getDefaultMiddleware({
        serializableCheck: false
    }),
});
