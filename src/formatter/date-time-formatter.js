export function getFormattedDate(date, pattern){
    if(date === undefined || pattern === undefined) {
        return "";
    }
    const dateFormat = require('dateformat');
    return dateFormat(Date.parse(date), pattern, true);
}
